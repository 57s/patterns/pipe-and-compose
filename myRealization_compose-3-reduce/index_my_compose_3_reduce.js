import { STRING_DIRTY } from '../data/data.js';

import { my_compose_3_reduce } from './my_compose_3_reduce.js';

import {
	makeEveryWordFirstLetterUpper,
	makeLettersLower,
	trim,
} from '../utils/functions.js';

const composeWith = my_compose_3_reduce(
	makeEveryWordFirstLetterUpper,
	makeLettersLower,
	trim
);

const result = composeWith(STRING_DIRTY);

console.log(result);
