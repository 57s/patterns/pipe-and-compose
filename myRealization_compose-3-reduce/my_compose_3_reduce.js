function my_compose_3_reduce(...functions) {
	return function (initValue) {
		return functions.reduceRight(
			(calculations, func) => func(calculations),
			initValue
		);
	};
}

export { my_compose_3_reduce };
