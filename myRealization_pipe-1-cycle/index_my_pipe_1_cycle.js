import { my_pipe_1_cycle } from './my_pipe_1_cycle.js';
import { increment } from '../utils/functions.js';

console.log(
	my_pipe_1_cycle(10, increment, increment, increment, increment, increment)
);
