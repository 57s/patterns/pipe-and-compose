function my_pipe_1_cycle(value, ...functions) {
	let result = value;

	for (let index = 0; index < functions.length; index++) {
		result = functions[index](result);
	}

	return result;
}

export { my_pipe_1_cycle };
