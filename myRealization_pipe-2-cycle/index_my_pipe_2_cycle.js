import { my_pipe_2_cycle } from './my_pipe_2_cycle.js';
import { division, addition } from '../utils/functions.js';

const result = my_pipe_2_cycle(
	number => addition(number, 100),
	number => division(number, 2)
)(10);

console.log(result);
