function my_pipe_2_cycle(...functions) {
	// Вернуть функцию которая принимает инициализирующее значение
	// Далее она запустить все функции с лева на права передав результат прошлого вычисления
	return function (initializationValue) {
		let result = initializationValue;

		for (let index = 0; index < functions.length; index++) {
			result = functions[index](result);
		}

		return result;
	};
}

export { my_pipe_2_cycle };
