// Test Functions
export const increment = number => ++number;
export const decrement = number => --number;
export const addition = (numberA, numberB) => numberA + numberB;
export const multiplication = (numberA, numberB) => numberA * numberB;
export const division = (numberA, numberB) => numberA / numberB;
export const iff = (expression, that, other) => (expression ? that : other);

export const power = Math.pow;
export const log = Math.log;
export const makeLettersUpper = string => string.toUpperCase();
export const makeLettersLower = string => string.toLowerCase();
export const makeFirstLetterUpper = word =>
	word.charAt(0).toUpperCase() + word.slice(1);

export const makeEveryWordFirstLetterUpper = string => {
	return string
		.split(' ')
		.map(item => makeFirstLetterUpper(item))
		.join(' ');
};

export const loop = function (from, to, func) {
	for (let index = from; index <= to; index++) {
		func(index);
	}
};

export const trim = function (string) {
	return string.replace(/^\s+|\s+$/g, '');
};
