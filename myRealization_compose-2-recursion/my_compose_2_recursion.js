function my_compose_2_recursion(...functions) {
	return function (initValue) {
		const currentCalculations = functions.at(-1)(initValue);

		if (functions.length === 1) return currentCalculations;

		return my_compose_2_recursion(...functions.slice(0, -1))(
			currentCalculations
		);
	};
}

export { my_compose_2_recursion };
