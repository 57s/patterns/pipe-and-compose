import { STRING_DIRTY } from '../data/data.js';

import {
	trim,
	makeLettersLower,
	makeFirstLetterUpper,
} from '../utils/functions.js';
import { my_compose_2_recursion } from './my_compose_2_recursion.js';

const composeWith = my_compose_2_recursion(
	makeFirstLetterUpper,
	makeLettersLower,
	trim
);

const result = composeWith(STRING_DIRTY);

console.log(result);
