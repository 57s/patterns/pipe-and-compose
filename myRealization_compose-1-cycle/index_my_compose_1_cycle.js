import { STRING_UPPER } from '../data/data.js';

import {
	makeLettersLower,
	makeEveryWordFirstLetterUpper,
} from '../utils/functions.js';
import { my_compose_1_cycle } from './my_compose_1_cycle.js';

const result = my_compose_1_cycle(
	makeEveryWordFirstLetterUpper,
	makeLettersLower
)(STRING_UPPER);

console.log(result);
