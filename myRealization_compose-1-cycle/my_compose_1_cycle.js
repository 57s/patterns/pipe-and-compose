function my_compose_1_cycle(...functions) {
	return function (initValue) {
		let calculations = initValue;

		for (let index = functions.length - 1; index >= 0; index--) {
			const currentCalculation = functions[index](calculations);
			calculations = currentCalculation;
		}

		return calculations;
	};
}

export { my_compose_1_cycle };
