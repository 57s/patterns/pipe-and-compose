import { my_pipe_4_reduce } from './my_pipe_4_reduce.js';
import { division, addition } from '../utils/functions.js';

const result = my_pipe_4_reduce(
	number => addition(number, 100),
	number => division(number, 2)
)(10);

console.log(result);
