function my_pipe_4_reduce(...functions) {
	return function (initializationValue) {
		return functions.reduce(
			(result, func) => func(result),
			initializationValue
		);
	};
}

export { my_pipe_4_reduce };
