function my_pipe_3_recursion(...functions) {
	return function (initialValue) {
		const currentResult = functions[0](initialValue);
		if (functions.length === 1) return currentResult;

		return my_pipe_3_recursion(...functions.slice(1))(currentResult);
	};
}

export { my_pipe_3_recursion };
