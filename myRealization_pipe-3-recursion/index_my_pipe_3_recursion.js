import { my_pipe_3_recursion } from './my_pipe_3_recursion.js';
import { decrement, multiplication, division } from '../utils/functions.js';

const composing = my_pipe_3_recursion(
	decrement,
	number => multiplication(number, 5),
	number => division(number, 2)
);

const result = composing(101);
console.log(result);
